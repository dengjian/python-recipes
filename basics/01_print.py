"""
Just for the sake of teaching you the first program anyone would write in any programming language, 
the hello world program
"""

print "Hello world"


print """
print 'Hello world'

Yes, that's it.
Now lets do some advanced formatting.
"""

alice = "Python"
bob	 = 40
mike = 100

print "%s is %d%% object oriented; sorry %d%%." % (alice, bob, mike)

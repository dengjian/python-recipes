carly_marks = [20, 19, 14, 18, 20]
"""
Doesn't it makes sense, if we could figure out which mark belongs to which subject?
Thanks to python dictionaries. You can do that.
"""

carly_marks = {
	'English': 20,
	'Maths': 19,
	'Chemistry': 14,
	'Physics': 18,
	'Programming': 20
}

print "Subject names are the keys and the marks are values."

print carly_marks

print "carly_marks['Programming'] is %d" % carly_marks['Programming']
print "or carly_marks.get('Programming') is %d" % carly_marks.get('Programming')
print "Second one doesn't complain and returns blank if the key doesn't exist"
print """\nIf we need to insert into a dictionary???"""

carly_marks['Humanities'] = 19

print "carly_marks['Humanities'] = %d" % carly_marks['Humanities']

print """\nAs simple as that. use carly_marks.keys() to get list of keys
or carly_marks.values() to get list of values
or carly_marks.iteritems() to iterate over the keys and values"""
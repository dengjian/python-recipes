alice_secret = 31
bob_secret = 11

"""
alice_secret and bob_secret is married. Now they got carly, answer to the ultimate question.
"""

carly = alice_secret + bob_secret

print "Carly holds %d, sum of %d and %d" % (carly, alice_secret, bob_secret)

"""
Similarly, python is capable of most of common operations out of the box.
For everything else, there is math module.
"""

print "%d - %d is %d" % (alice_secret, bob_secret, alice_secret - bob_secret)
print "%d - %d is %d" % (bob_secret, alice_secret, bob_secret - alice_secret)
print "%d * %d is %d" % (alice_secret, bob_secret, alice_secret * bob_secret)

print "now look at this"

print "%d / %d is %f" % (alice_secret, bob_secret, alice_secret / bob_secret)
print "now, that's bad! Integer divison in python omits the decimal part. Try this instead"
print "%d / %f is %f" % (alice_secret, float(bob_secret), alice_secret / float(bob_secret))

print "%d %% %d is %d" % (alice_secret, bob_secret, alice_secret % bob_secret)

print "(%d + %d)*(%d - %d) is %d"  % (alice_secret, bob_secret, alice_secret, bob_secret, \
		((alice_secret+bob_secret)*(alice_secret-bob_secret)))

"""
See the \ at the end of line 29. This is to split long lines of code into muliple lines.
"""
bob_age = 51  #will create a class soon. Don't worry.
alice_age = 51

if alice_age == bob_age:
	print "Alice is as old as Bob."
else:
	print "Alice and Bob have different ages."

alice_age = 49
bob_age = 51

if alice_age < bob_age:
	print "Alice is younger than Bob."
else:
	print "Bob is younger than Alice."

bob_car = True
if bob_car:
	"""
	Look at the expression, it doesn't have an operator or comparision.
	Implicitly, if the expression is evaluating to a Truth value, 
	it is taken as True.
	"""
	print "Bob have a car."

if alice_age < bob_age and bob_car:
	print "Alice is younger than Bob, and Bob have a car."

print """
We have <, ==, >, >=, <=, !=, or, and, not
Try elif for yourself.
"""
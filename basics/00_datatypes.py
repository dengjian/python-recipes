print """
Meet alice and bob. They'll be with you till the end of this course.
They'll help you understand the basic concepts of python easy and simple.
"""

alice = "Hello world"  # alice is now a name(refered mostly as a variable), with the value Hello world in it"
alice = 'Hello world'  # this is also correct

print "alice holds ", alice  # bad way of printing, will teach you better way to do it in 02.
print "alice[1:5] is ", alice[1:5]
print "alice.isalnum() ", alice.isalnum()  # False: why? space is not alnum
print "alice.upper() is ", alice.upper()

bob = 10  # bob is an integer type variable
bob = 10.0  # now bob is a float variable 

print "bob holds ", bob
print """
Python is dynamically typed. So, there is no need to specify the type of the variable declared upfront.
If you use C/C++, you know this is a big advantage. 
"""

print "Type of alice is ", type(alice)
print "Type of bob is ", type(bob)

print """
Everything in python is an object, even above mentioned datatypes. 
There exist awesome datastructures like list and dictionaries, and we'll see it in later tutorials.
"""
alice_greetings = ['hello', 'hi', 'hey', 'morning', 'fine', 'hello', 'morning', 'hi']
print "alice_greetings: ", alice_greetings
print "set(alice_greetings): ", set(alice_greetings)
print "set(alice_greetings).pop(): ", set(alice_greetings).pop()
print "set(alice_greetings).issubset(alice_greetings)", \
			set(alice_greetings).issubset(alice_greetings)
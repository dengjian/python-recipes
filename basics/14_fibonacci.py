import sys
def fibonacci(limit):
	"""
	>>> fibonacci(1000)
	[1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89, 144, 233, 377, 610, 987]
	"""
	fib_series = []
	a, b = 0, 1
	while b < limit:
		fib_series.append(b)
		a, b = b, a+b
	return fib_series
fib_list = fibonacci(int(sys.argv[1]))
print sys.argv
print fib_list
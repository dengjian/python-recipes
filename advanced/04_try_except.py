try:
	q = 1/0
except:
	print "Division by Zero"

"""
---------------------------
Let's create a custom exception, and raise it, and then handle it.
"""

import datetime


class NegativeAgeError(Exception):
    def __init__(self, message):
        super(NegativeAgeError, self).__init__(message)
        self.message = message


class Human:
	"""
	A class for modelling humans with name and age

	http://bit.ly/15iXWlV
	"""
	def check_age(self, age):
		"""
		validate if age is valid
		"""
		if age > 0:
			return age
		else:
			raise NegativeAgeError('Age is negative! Not possible.')

	def __init__(self, name="", age=0):
		if name:
			self.name = name
		if self.check_age(age):
			self.age = age

try:
	cindy = Human('Cindy', -1)
except NegativeAgeError as msg:
	print msg
"""
Decoration for functions is a way in python is add additional 
capabilities while a function is being invoked.
These decorator functions are called prior to the 
execution of the function, and there by manipulating the 
flow of control around the function. 
For eg., controlling access to a specific function.
"""
def decoration(func):
	"""
	Decorator function definition takes function as an argument.
	"""
	def inner(*args, **kwargs):
		print "Decorating ..."
		funt = func(*args, **kwargs)
		print "Decoration after function"
		return funt
	return inner

@decoration
def test_function():
	"""
	@decoration just above the function definition calls the 
	decorator function of the same name with the test_function as
	an argument.
	"""
	print "Text from function"

if __name__ == '__main__':
	test_function()
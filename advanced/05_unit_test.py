import unittest

class Calculator(object):
	def __init__(self, x, y):
		self.x = x
		self.y = y
	def add(self):
		return self.x + self.y
	def subtract(self):
		return self.x - self.y
	def multiply(self):
		return self.x * self.y
	def divide(self):
		return self.x / float(self.y)


class TestMCalculator(unittest.TestCase):

    def setUp(self):
        self.calculator = Calculator(100, 77)

    def test_add(self):
        '''
        answer should be the sum of x and y.
        '''
        self.assertEqual(
                self.calculator.add(), 177)

    def test_subtract(self):
        '''
        answer should be x - y.
        '''
        self.assertEqual(
                self.calculator.subtract(), 23)

if __name__ == '__main__':
    unittest.main()
"""
Open the file in w - write mode - flushes all contents if any, 
else touches a new file.
"""

f_inst = open("newfile", 'w')
f_inst.write("It works!")
f_inst.close()

"""
Open the file in a - append mode - preserves all contents in the file.
"""
f_inst = open("newfile", 'a')
f_inst.write("\nYes it really does!")
f_inst.close()

"""
Open the file in r - read mode - read only mode - throws exception 
if file doesn't exist.
"""
f_inst = open("newfile", 'r')
print f_inst.read()
f_inst.close()

